package com.YasiuraRoman.view;

@FunctionalInterface
public interface Printable {
    void print();
}
