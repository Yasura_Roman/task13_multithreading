package com.YasiuraRoman.view;

import com.YasiuraRoman.part1.PartOne;
import com.YasiuraRoman.part2.PartTwo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class GeneralView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");

    public GeneralView() {
        PartOne partOne = new PartOne();
        PartTwo partTwo = new PartTwo();
        menu = new LinkedHashMap<>();
        createMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", partOne::pingPong);
        methodsMenu.put("2", () -> {
            logger.info("input number");
            int num;
            try {
                num = input.nextInt();
            } catch (Exception e) {
                logger.error(e.getMessage());
                return;
            }
            partOne.findFibonacciNumber(num);
        });
        methodsMenu.put("3", () -> {
            logger.info("input number");
            int num;
            try {
                num = input.nextInt();
            } catch (Exception e) {
                logger.error(e.getMessage());
                return;
            }
            partOne.findFibonacciNumberWithExecutors(num);
        });
        methodsMenu.put("4", () -> {
            int start;
            int stop;
            logger.info("input start number");
            try {
                start = input.nextInt();
                logger.info("input stop number");
                stop = input.nextInt();
            } catch (Exception e) {
                logger.error(e.getMessage());
                return;
            }
            partOne.getSumOfFibonacciNumbers(start,stop);
        });
        methodsMenu.put("5", () -> {
            partOne.executeRandomSleepThreads(3);
        });
        methodsMenu.put("6", partOne::showPipedThread);
        methodsMenu.put("7", partOne::executeCriticalSectionExample);
        methodsMenu.put("8", partTwo::executeLockExample);
        methodsMenu.put("9", partTwo::showPipedThread);
        methodsMenu.put("10", partTwo::testCustomLock);
    }

    private void createMenu(){
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
