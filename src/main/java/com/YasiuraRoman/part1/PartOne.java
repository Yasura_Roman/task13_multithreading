package com.YasiuraRoman.part1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PartOne {
    private Object sync = new Object();
    private Logger logger = LogManager.getLogger();
    private Random random = new Random();
    public void pingPong() {
        Thread pingThread = pingPongThread("Ping");
        Thread pongThread = pingPongThread("Pong");
        pingThread.start();
        pongThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e);
        }
        pingThread.interrupt();
        pongThread.interrupt();
    }

    private Thread pingPongThread(String text) {
        return new Thread(() -> {
            synchronized (sync) {
                while (!Thread.interrupted()) {
                    try {
                        sync.notify();
                        logger.info(text);
                        sync.wait();
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        });
    }

    public void findFibonacciNumber(int index) {
        for (int i = 0; i < 5; i++) {
            new Thread(
                    () -> logger.info(  calculateFibonacciNumber(index + random.nextInt(10)))
            ).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    public void findFibonacciNumberWithExecutors(int index) {
        final int threadsNumber = 8;

        logger.info("__________fixedThreadPool__________");
        ExecutorService fixedExecutorService = Executors.newFixedThreadPool(threadsNumber);
        for (int i = 0; i < threadsNumber; i++) {
            fixedExecutorService.submit(
                    getFibonacciRunnable(index + i));
        }

        logger.info("__________singleThreadPool__________");
        ExecutorService singleThreadExecutorService = Executors.newSingleThreadExecutor();
        for (int i = 0; i < threadsNumber; i++) {
            singleThreadExecutorService.submit(
                    getFibonacciRunnable(index + i));
        }

        logger.info("__________singleThreadPoolScheduled__________");
        ScheduledExecutorService singleThreadScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        for (int i = 0; i < threadsNumber; i++) {
            singleThreadScheduledExecutorService.schedule(
                    getFibonacciRunnable(index + i),1, TimeUnit.SECONDS);
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        fixedExecutorService.shutdown();
        singleThreadExecutorService.shutdown();
        singleThreadScheduledExecutorService.shutdown();
    }

    private Runnable getFibonacciRunnable(int index) {
        return () -> {
            logger.info("Thread " + Thread.currentThread().getId() +
                    " Start time " + System.nanoTime());
            logger.info(calculateFibonacciNumber(index));
            logger.info("Thread " + Thread.currentThread().getId() +
                    " Stop time " + System.nanoTime());
        };
    }

    public int getSumOfFibonacciNumbers(int startIndex, int amountOfNumbers) {

        Callable<Integer> sumOfFibonacciNumbers = () -> {
            int sum = 0;
            for (int i = 0; i < amountOfNumbers; i++) {
                sum += calculateFibonacciNumber(startIndex + i);
            }
            return sum;
        };

        try {
            return sumOfFibonacciNumbers.call();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return 0;
    }

    public void executeRandomSleepThreads(int numberOfThreads) {
        for (int i = 0; i < numberOfThreads; i++) {
            new Thread(() -> {
                int sleepTime = random.nextInt(10) + 1;
                logger.info("Start thread with " + sleepTime + " seconds sleep");
                try {
                    Thread.sleep(sleepTime * 1000);
                } catch (InterruptedException e) {
                    logger.error(e.getMessage());
                }
                logger.info("Exit thread with " + sleepTime + " seconds sleep");
            }).start();
        }
    }

    public void executeCriticalSectionExample() {
        logger.info("__________one monitor__________");
        criticalSectionWithOneMonitor();
        logger.info("__________few monitor__________");
        criticalSectionWithFewMonitor();
    }

    private Runnable synchronizedThread(Object monitor, AtomicInteger a){
        return () -> {
            synchronized(monitor) {
                a.addAndGet(10);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    logger.error(e.getMessage());
                }
            }
        };
    }

    private void criticalSectionWithOneMonitor() {
        Object monitor = new Object();
        Thread[] threads = new Thread[3];
        AtomicInteger a = new AtomicInteger();

        for (int i = 0; i <threads.length ; i++) {
            threads[i] = new Thread(synchronizedThread(monitor,a));
        }
        for (Thread thread :threads
        ) {
            thread.start();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
        logger.info(a);
    }

    private void criticalSectionWithFewMonitor() {
        Object[] monitors = new Object[3];
        Thread[] threads = new Thread[3];
        AtomicInteger a = new AtomicInteger();

        for (int i = 0; i <threads.length ; i++) {
            threads[i] = new Thread(synchronizedThread(monitors[i],a));
        }
        for (Thread thread :threads
        ) {
            thread.start();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
        logger.info(a);
    }

    private int calculateFibonacciNumber(int index) {
        if (index <= 1) {
            return index;
        }
        return calculateFibonacciNumber(index-1) + calculateFibonacciNumber(index - 2);
    }

    public void showPipedThread() {
        String text = "some text : Contrary to popular belief, Lorem Ipsum is not simply random text.\n  ";
        PipedInputStream pipedInputStream = new PipedInputStream();
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        try {
            pipedInputStream.connect(pipedOutputStream);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        Thread writerThread = getOutputPipeThread(pipedOutputStream, text);
        Thread readerThread = getInputPipeThread(pipedInputStream);
        writerThread.start();
        readerThread.start();

        try {
            writerThread.join();
            readerThread.join();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    private Thread getInputPipeThread(PipedInputStream pipedInputStream) {
        return new Thread(() -> {
            int c;
            try {
                while ((c = pipedInputStream.read()) != -1) {
                    System.out.print((char)c);
                    Thread.sleep(100);
                }
                pipedInputStream.close();
            } catch (IOException | InterruptedException e) {
                logger.error(e.getMessage());
            }
        });
    }

    private Thread getOutputPipeThread(PipedOutputStream pipedOutputStream, String text) {
        return new Thread(() -> {
            try {
                for (int c: text.toCharArray()) {
                    pipedOutputStream.write(c);
                }
                pipedOutputStream.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        });
    }
}
