package com.YasiuraRoman.part2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class PartTwo {

    private Logger logger = LogManager.getLogger();

    public void executeLockExample() {
        logger.info("__________one lock__________");
        criticalSectionWithOneLock();
        logger.info("__________few locks__________");
        criticalSectionWithFewLocks();
    }

    private Runnable synchronizedThread(ReentrantLock lock, AtomicInteger a){
        return () -> {
            lock.lock();
            a.addAndGet(10);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
            lock.unlock();
        };
    }

    private void criticalSectionWithOneLock() {
        ReentrantLock lock = new ReentrantLock();
        Thread[] threads = new Thread[3];
        AtomicInteger a = new AtomicInteger();

        for (int i = 0; i <threads.length ; i++) {
            threads[i] = new Thread(synchronizedThread(lock,a));
        }
        for (Thread thread :threads
        ) {
            thread.start();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        logger.info(a);
    }

    private void criticalSectionWithFewLocks() {
        ReentrantLock[] locks = new ReentrantLock[3];
        Thread[] threads = new Thread[3];
        AtomicInteger a = new AtomicInteger();

        for (int i = 0; i <threads.length ; i++) {
            threads[i] = new Thread(synchronizedThread(locks[i],a));
        }
        for (Thread thread :threads
        ) {
            thread.start();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        logger.info(a);
    }

    public void showPipedThread() {
        BlockingQueue<Character> symbolsQueue = new LinkedBlockingQueue<>();
        String text = "some text : Contrary to popular belief, Lorem Ipsum is not simply random text.\n";
        Thread writerThread = getWriterThread(symbolsQueue, text);
        Thread readerThread = getReaderThread(symbolsQueue);
        writerThread.start();
        readerThread.start();

        try {
            writerThread.join();
            readerThread.join();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    private Thread getReaderThread(BlockingQueue<Character> queue) {
        return new Thread(() -> {
            while (!queue.isEmpty()) {
                logger.info(queue.poll());
            }
        });
    }

    private Thread getWriterThread(BlockingQueue<Character> queue, String text) {
        return new Thread(() -> {
            for (char c : text.toCharArray()) {
                queue.add(c);
            }
        });
    }

    private Thread readWriteLock(MyReadWriteLock.Lock readWriteLock,AtomicInteger a){
        return new Thread( () -> {
            readWriteLock.lock();
            a.addAndGet(10);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
            readWriteLock.unlock();
        });
    }

    public void testCustomLock() {
        MyReadWriteLock readWriteLock = new MyReadWriteLock(logger);
        MyReadWriteLock.WriteLock writeLock = readWriteLock.getWriteLock();
        MyReadWriteLock.ReadLock readLock = readWriteLock.getReadLock();
        AtomicInteger a = new AtomicInteger();

        Thread thread1 = readWriteLock(readLock,a);
        Thread thread2 = readWriteLock(writeLock,a);
        Thread thread3 = readWriteLock(readLock,a);

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }
}
