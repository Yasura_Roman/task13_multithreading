package com.YasiuraRoman.part2;

import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;

public class MyReadWriteLock {
    private static final String READ = "r";
    private static final String WRITE = "w";
    private final ReadLock readerLock;
    private final WriteLock writerLock;
    private AtomicInteger readCount = new AtomicInteger(0);
    private AtomicInteger writeCount = new AtomicInteger(0);
    private Logger logger;

    public MyReadWriteLock(Logger logger) {
        this.logger = logger;
        readerLock = new ReadLock();
        writerLock = new WriteLock();
    }

    public WriteLock getWriteLock() {
        return writerLock;
    }

    public ReadLock getReadLock() {
        return readerLock;
    }

    public class ReadLock implements Lock{

        public void lock() {
            if (writeCount.get() == 0) {
                readCount.incrementAndGet();
            } else {
                while (writeCount.get() != 0) {
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        }

        public void unlock() {
            readCount.decrementAndGet();
        }

    }

    public class WriteLock implements Lock {

        public void lock() {
            if (writeCount.get() == 0 && readCount.get() == 0) {
                writeCount.incrementAndGet();
            } else {
                while (writeCount.get() != 0 && readCount.get() != 0) {
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        }

        public void unlock() {
            writeCount.decrementAndGet();
        }

    }

    public interface Lock{
        void lock();
        void unlock();
    }
}
